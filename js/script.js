// Слайдер на главной странице

jQuery(document).ready(function() {

    $('.main_slider').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slide: '.main_slide_item',
        slidesToShow: 1,
        slidesToScroll: 1,
        touchMove: false

    });

    $('.benefits_slider').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slide: '.benefit_slide_item',
        slidesToShow: 1,
        slidesToScroll: 1,
        touchMove: false

    });

    $('.our_partners_carousel').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slide: '.item_our_partner',
        slidesToShow: 5,
        slidesToScroll: 1,
        touchMove: false

    });

    $('.reviews_carousel').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slide: '.rev_carousel',
        slidesToScroll: 1,
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '28px',
        touchMove: false

    });

    $('.review_item_gallery_container').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slide: '.bl_carousel_item',
        vertical: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        touchMove: false

    });

    $('.reviews_carousel').on('click', '.slick-slide', function(){
        if ($(this).prev('.slick-active').length) {
            $('.reviews_carousel .slick-next').trigger('click');
            return;
        }
        if ($(this).next('.slick-active').length) {
            $('.reviews_carousel .slick-prev').trigger('click');
            return;
        }
    });

    $('.blog_read_all').on('click', function(){

        $(this).prev('.blog_post_tx_hide').slideToggle(function() {
            $(this).next('.blog_read_all').text(
                $(this).is(':visible') ? "Свернуть" : "Читать полностью"
            );
        });

    });

    $('.read_all').on('click', function(){

        $(this).prev('.review_tx_hide').slideToggle(function() {
            $(this).next('.read_all').text(
                $(this).is(':visible') ? "Свернуть" : "Читать полностью"
            );
        });

    });

    $('.but_show_all_photo').on('click', function(){

        $(this).hide();
        $(this).next('.album_all_photo').find('.hide_show_photo').show();
        $('.but_hide_sp_photo').show();

    });

    $('.but_hide_sp_photo').on('click', function(){

        $(this).hide();
        $(this).parent().parent().hide();
        $(this).parent().parent().parent().prev().show();

    });

    (function(){
        var animate = function(){
            var $that = this,
                current = parseInt($that.text());
            if (current == $that.data('final')) { return; }
            setTimeout(function(){
                $that.text(++current);
                animate.bind($that)();
            }, 50);
        };
        $(window).on('load scroll', function(){
            $('.count').each(function(){
                var $item = $(this);

                if ($item.hasClass('animated') || !$item.is(':visible')) { return true; }

                $item.addClass('animated').data('final', $item.text()).text(0);
                animate.bind($item)();
            });
        });
    })();
});

//Слайдер модальной странице
jQuery(document).ready(function() {

    $('.main_modal_slider').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slide: '.main_modal_slide_item',
        slidesToShow: 1,
        slidesToScroll: 1,
        touchMove: false

    });

});

//Скрипт замены селектов

        (function($) {
            $(function() {

                $('select').selectbox();

                $('#add').click(function(e) {
                    $(this).parents('div.section').append('<br /><br /><select><option>-- Выберите --</option><option>Пункт 1</option><option>Пункт 2</option><option>Пункт 3</option><option>Пункт 4</option><option>Пункт 5</option></select>');
                    $('select').selectbox();
                    e.preventDefault();
                })

                $('#add2').click(function(e) {
                    var options = '';
                    for (i = 1; i <= 5; i++) {
                        options += '<option>Option ' + i + '</option>';
                    }
                    $(this).parents('div.section').find('select').each(function() {
                        $(this).append(options);
                    })
                    $('select').trigger('refresh');
                    e.preventDefault();
                })

                $('#che').click(function(e) {
                    $(this).parents('div.section').find('option:nth-child(5)').attr('selected', true);
                    $('select').trigger('refresh');
                    e.preventDefault();
                })

            })
        })(jQuery)


//Слайдер листания партнеров

        $('.slider-part').slick({
            dots: false,
            infinite: true,
            variableWidth: true,
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
		
		
		



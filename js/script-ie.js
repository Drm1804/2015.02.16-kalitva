//Табы страницы "услуги" и "стать партнером"

(function($) {
    $(function() {

        $('div.uslugi').each(function() {
            $(this).find('label.label_uslugi').each(function(i) {
                $(this).click(function(){
                    $(this).addClass('current').siblings().removeClass('current')
                        .parents('div.uslugi').find('div.descript_uslug').eq(i).fadeIn(150).siblings('div.descript_uslug').hide();
                });
            });
        });

    })
})(jQuery)

//Табы для страницы "Написать нам"
(function($) {
    $(function() {

        $('div.contact_form').each(function() {
            $(this).find('label.label_two_saubject').each(function(i) {
                $(this).click(function(){
                    $(this).addClass('current').siblings().removeClass('current')
                        .parents('div.contact_form').find('div.form_napis').eq(i).fadeIn(150).siblings('div.form_napis').hide();
                });
            });
        });

    })
})(jQuery)


function tabContact(tab){
    if (tab == 1){
        document.getElementById('first_box').style.display = 'block';
        document.getElementById('second_box').style.display = 'none';
        document.getElementById('third_box').style.display = 'none';
        document.getElementById('label_for_first_tab').style.textDecoration = 'underline';
        document.getElementById('label_for_second_tab').style.textDecoration = 'none';
        document.getElementById('label_for_third_tab').style.textDecoration = 'none';
    }
    else if (tab == 2){
        document.getElementById('first_box').style.display = 'none';
        document.getElementById('second_box').style.display = 'block';
        document.getElementById('third_box').style.display = 'none';
        document.getElementById('label_for_first_tab').style.textDecoration = 'none';
        document.getElementById('label_for_second_tab').style.textDecoration = 'underline';
        document.getElementById('label_for_third_tab').style.textDecoration = 'none';

    }
    else{
        document.getElementById('first_box').style.display = 'none';
        document.getElementById('second_box').style.display = 'none';
        document.getElementById('third_box').style.display = 'block';
        document.getElementById('label_for_first_tab').style.textDecoration = 'none';
        document.getElementById('label_for_second_tab').style.textDecoration = 'none';
        document.getElementById('label_for_third_tab').style.textDecoration = 'underline';

    }
}
function tabMaker(tab){
    if (tab == 1){
        document.getElementById('tabMarkerFormOne').style.display = 'block';
        document.getElementById('tabMarkerFormTwo').style.display = 'none';
        document.getElementById('tabMarkerFormThree').style.display = 'none';
        document.getElementById('label_two_first_subject').style.textDecoration = 'underline';
        document.getElementById('label_two_second_subject').style.textDecoration = 'none';
        document.getElementById('label_two_third_subject').style.textDecoration = 'none';
    }
    else if (tab == 2){
        document.getElementById('tabMarkerFormOne').style.display = 'none';
        document.getElementById('tabMarkerFormTwo').style.display = 'block';
        document.getElementById('tabMarkerFormThree').style.display = 'none';
        document.getElementById('label_two_first_subject').style.textDecoration = 'none';
        document.getElementById('label_two_second_subject').style.textDecoration = 'underline';
        document.getElementById('label_two_third_subject').style.textDecoration = 'none';
    }
    else{
        document.getElementById('tabMarkerFormOne').style.display = 'none';
        document.getElementById('tabMarkerFormTwo').style.display = 'none';
        document.getElementById('tabMarkerFormThree').style.display = 'block';
        document.getElementById('label_two_first_subject').style.textDecoration = 'none';
        document.getElementById('label_two_second_subject').style.textDecoration = 'none';
        document.getElementById('label_two_third_subject').style.textDecoration = 'underline';
    }

}